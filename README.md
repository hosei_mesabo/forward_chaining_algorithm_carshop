Certainly! Here's the `README.md` as a Markdown source code:


# CarShop Inference System
```
The CarShop Inference System is a rule-based inference system that uses a pre-defined set of rules to determine characteristics about cars based on given facts.
```
## Prerequisites
```
Ensure you have Python 3 and pip3 installed on your system.
```

## Getting Started
```
Follow the steps below to set up and run the project:
```

### 1. Clone the Project

bash

> git clone <repository-url>

> cd <repository-dir>


Replace `<repository-url>` with the URL of this git repository and `<repository-dir>` with the name of the directory where the repository is cloned.

### 2. Install pip3 (if not installed)

For Linux:
\```bash```
```
sudo apt-get update
sudo apt-get install python3-pip
```

For macOS:
\```bash```
```
brew install python3
```

For Windows, the Python installer from the official website usually comes with pip3.

### 3. Create a Virtual Environment

It's recommended to create a virtual environment to manage dependencies.

```bash```
```
python3 -m venv venv
```

### 4. Activate the Virtual Environment

#### Linux/macOS:
```bash```
```
source venv/bin/activate
```

#### Windows:
```bash``` 
```
.\venv\Scripts\activate
```

### 5. Install Required Packages

```bash```
```
pip3 install -r requirements.txt
```

### 6. Run the Project

```bash```
```
python3 main.py
```

## Using the System

1. Enter the facts about the car in the provided text field.
2. Click "Add" to add the fact.
3. Click "Apply Rules" to process the facts and receive inferences based on the defined rules.

## License

This project is licensed under the MIT License. (You can change this if needed)

---

Using the System
Enter the facts about the car in the provided text field.
Click "Add" to add the fact.
Click "Apply Rules" to process the facts and receive inferences based on the defined rules.
License
This project is licensed under the MIT License. (You can change this if needed)

** The original txt file is in this directory under the name:
> L4_CarShop.txt