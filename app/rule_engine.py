rules = [
    {"if": ["my-car is inexpensive"], "then": "my-car is made in Japan"},
    {"if": ["my-car is small"], "then": "my-car is made in Japan"},
    {"if": ["my-car is expensive"], "then": "my-car is a foreign car"},
    {"if": ["my-car is big", "my-car needs a lot of gas"], "then": "my-car is a foreign car"},
    {"if": ["my-car is made in Japan", "my-car has Toyota's logo"], "then": "my-car is a Toyota"},
    {"if": ["my-car is made in Japan", "my-car is a popular car"], "then": "my-car is a Toyota"},
    {"if": ["my-car is made in Japan", "my-car has Honda's logo"], "then": "my-car is a Honda"},
    {"if": ["my-car is made in Japan", "my-car has a VTEC engine"], "then": "my-car is a Honda"},
    {"if": ["my-car is a Toyota", "my-car has several seats", "my-car is a wagon"], "then": "my-car is a Carolla Wagon"},
    {"if": ["my-car is a Toyota", "my-car has several seats", "my-car is a hybrid car"], "then": "my-car is a Prius"},
    {"if": ["my-car is a Honda", "my-car is stylish", "my-car has several color models", "my-car has several seats", "my-car is a wagon"], "then": "my-car is an Accord Wagon"},
    {"if": ["my-car is a Honda", "my-car has an aluminium body", "my-car has only 2 seats"], "then": "my-car is a NSX"},
    {"if": ["my-car is a foreign car", "my-car is a sports car", "my-car is stylish", "my-car has several color models", "my-car has a big engine"], "then": "my-car is a Lamborghini Countach"},
    {"if": ["my-car is a foreign car", "my-car is a sports car", "my-car is red", "my-car has a big engine"], "then": "my-car is a Ferrari F50"},
    {"if": ["my-car is a foreign car", "my-car is a good face"], "then": "my-car is a Jaguar XJ8"}
]

def apply_rules(facts):
    new_facts = []
    new_assertions = True
    while new_assertions:
        new_assertions = False
        for rule in rules:
            if all(fact in facts for fact in rule['if']) and rule['then'] not in facts:
                facts.append(rule['then'])
                new_assertions = True
                new_facts.append(rule['then'])
    return new_facts
