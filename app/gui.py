import tkinter as tk
from tkinter import ttk, messagebox
from app.facts_manager import load_facts, save_facts, reset_facts
from app.rule_engine import apply_rules

facts = load_facts()

def add_fact():
    input_facts = fact_entry.get().split(',')
    new_facts_added = False
    for fact in input_facts:
        fact = fact.strip()  # Remove any leading or trailing whitespaces
        if fact and fact not in facts:
            facts.append(fact)
            fact_list.insert(tk.END, fact)
            new_facts_added = True
    fact_entry.delete(0, tk.END)
    
    if new_facts_added:
        save_facts(facts)

def apply_inference():
    new_facts = apply_rules(facts)
    for fact in new_facts:
        output.insert(tk.END, f"Applied rule => {fact}")
        output.yview(tk.END)
    
    save_facts(facts)
    messagebox.showinfo("Result", "Finished applying rules!")

def reset_to_predefined():
    global facts
    facts = reset_facts()
    fact_list.delete(0, tk.END)
    output.delete(0, tk.END)
    messagebox.showinfo("Result", "Facts have been reset to predefined state!")

def initialize_gui():
    root = tk.Tk()
    root.title("CarShop Inference System")

    frame = ttk.Frame(root, padding="10")
    frame.grid(row=0, column=0, sticky=(tk.W, tk.E, tk.N, tk.S))

    fact_label = ttk.Label(frame, text="Add Fact:")
    fact_label.grid(row=0, column=0, sticky=tk.W, pady=5)

    global fact_entry
    fact_entry = ttk.Entry(frame, width=40)
    fact_entry.grid(row=0, column=1, pady=5)

    add_button = ttk.Button(frame, text="Add", command=add_fact)
    add_button.grid(row=0, column=2, pady=5, padx=10)

    global fact_list
    fact_list_label = ttk.Label(frame, text="Facts:")
    fact_list_label.grid(row=1, column=0, sticky=tk.W, pady=5)
    fact_list = tk.Listbox(frame, height=10, width=50)
    fact_list.grid(row=1, column=1, rowspan=4, pady=5)

    apply_rules_button = ttk.Button(frame, text="Apply Rules", command=apply_inference)
    apply_rules_button.grid(row=5, column=1, pady=20)

    reset_button = ttk.Button(frame, text="Reset to Predefined", command=reset_to_predefined)
    reset_button.grid(row=5, column=2, pady=20, padx=10)

    global output
    output_label = ttk.Label(frame, text="Output:")
    output_label.grid(row=6, column=0, sticky=tk.W, pady=5)
    output = tk.Listbox(frame, height=10, width=50)
    output.grid(row=6, column=1, rowspan=4, pady=5)

    return root
