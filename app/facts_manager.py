import os

FACTS_FILE = "facts_db.txt"

def load_facts():
    if not os.path.exists(FACTS_FILE):
        return []
    with open(FACTS_FILE, "r") as file:
        return [line.strip() for line in file.readlines()]

def save_facts(facts):
    with open(FACTS_FILE, "w") as file:
        for fact in facts:
            file.write(fact + "\n")

def reset_facts():
    if os.path.exists(FACTS_FILE):
        os.remove(FACTS_FILE)
    return []
