from app.gui import initialize_gui

if __name__ == "__main__":
    root = initialize_gui()
    root.mainloop()
